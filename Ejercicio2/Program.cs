﻿using System;

namespace Ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {            
            Carro miCarro = new Carro();

            miCarro.color = "Rojo";
            miCarro.marca = "Chevrolet";
            miCarro.numeroPuertas = 5;
            miCarro.anyModelo = 2013;
            miCarro.precio = 150000;
            miCarro.miMotor.asignarNumeroCilindros(4);
            miCarro.miMotor.asignarTipoCarburador("Inyección"); 
            miCarro.miMotor.TipoCombustible = "Gasolina";             
            miCarro.obtenerCaracteristicas();
            miCarro.miTanque.llenarTanque((decimal)40.0);                        
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();             
        }
    }

    class Tanque {
        private decimal cantidadLitrosCombustible;
        const decimal MAXIMO_LITROS = 40;
        public Tanque() {
            cantidadLitrosCombustible = 0;
        }
        public void vaciarTanque(decimal cantidadCombustible) {
            if (cantidadCombustible <= cantidadLitrosCombustible) {                 
                cantidadLitrosCombustible -= cantidadCombustible;                
                Console.WriteLine("Total de Combustible: "+cantidadLitrosCombustible.ToString());
            } else  {
                cantidadLitrosCombustible = 0;
                Console.WriteLine("Tanque vacio!: ");
            }
        }
        public void llenarTanque(decimal cantidadCombustible) {             
            if (cantidadCombustible > 0){
                if ( (cantidadCombustible + cantidadLitrosCombustible) <= MAXIMO_LITROS){
                    cantidadLitrosCombustible += cantidadCombustible;
                    Console.WriteLine("Total de combustible :" + cantidadLitrosCombustible.ToString());
                } else {
                   cantidadLitrosCombustible = MAXIMO_LITROS;
                   Console.WriteLine("Tanque lleno!: ");
                }
            }               
        }
        public decimal obtenerLitros() {
           return cantidadLitrosCombustible;
        }
    }

    class Motor {
        private int numeroCilindros;
        private string tipoCarburador;
        public string TipoCombustible {get; set;}
        public int obtenerNumeroCilintros() {
            return numeroCilindros;
        }
        public void asignarNumeroCilindros(int cantidad ) {
           numeroCilindros = cantidad;   
        }
        public string obtenerTipoCarburador() {
            return tipoCarburador;
        }
        public void asignarTipoCarburador(string tipoCarburador) {
          this.tipoCarburador = tipoCarburador; 
        }
        public void consumirCombustible(ref Tanque tanque, decimal cantidadLitros) {             
            if ( tanque != null ) {
                 tanque.vaciarTanque(cantidadLitros*(decimal)this.numeroCilindros);
            }
        }
    }
    class Carro {
        public string color;
        public string marca;
        public int anyModelo;
        public int numeroPuertas;
        public decimal precio;
        public Tanque miTanque;
        public Motor miMotor;
        public Carro() {
            miMotor = new Motor();
            miTanque = new Tanque();
        }
        public void acelerar() {
            Console.WriteLine("Estoy acelerando");   
            miMotor.consumirCombustible(ref miTanque, (decimal)0.4);    
            Console.WriteLine("Remanente de combustible :" + miTanque.obtenerLitros().ToString());
        }
        public void frenar() {
            Console.WriteLine("Estoy frenando");
        }
        public void obtenerCaracteristicas() {
            Console.WriteLine("Caracteristicas del auto");
            Console.WriteLine("Color :" + color);
            Console.WriteLine("Marca :" + marca);
            Console.WriteLine("Año del modelo :"+ anyModelo.ToString());
            Console.WriteLine("Número de puertas " + numeroPuertas.ToString());
            Console.WriteLine("Precio :" + precio.ToString());
            Console.WriteLine("Tipo de carburador :" + miMotor.obtenerTipoCarburador());
            Console.WriteLine("Número de cilindros : " + miMotor.obtenerNumeroCilintros().ToString());                         
            Console.WriteLine("Tipo de combustible :" + miMotor.TipoCombustible);
        }
    }
}
